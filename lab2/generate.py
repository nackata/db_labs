import psycopg2
import random
import dateparser
from datetime import datetime

from model import Model


def gen_str(range_from, range_to) -> str:
    letters = [chr(x) for x in range(ord('a'), ord('z')+1)]

    length = random.randint(range_from, range_to)
    return ''.join([random.choice(letters) for _ in range(length)])


model = Model()

for _ in range(5):
    owner = random.randint(1, 2)
    name = gen_str(5, 15)
    free = random.randint(0, 1) == 0

    upload_date = dateparser.parse("%s days ago" % random.randrange(0, 10))
    model.cur.execute("INSERT INTO \"Book\" (publisher, name, free, date) VALUES (%s, %s, %s, %s);", (owner, name, free, upload_date))
    model.commit()
