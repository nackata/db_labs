import psycopg2
from model import Model
from view import View

from functools import partial
from typing import Optional, List, Any, Dict, Callable


def transpose(titles: List[str], rows: List[List[Any]]) -> Dict[str, List[Any]]:
    dict = {k: [] for k in titles}

    for i in range(len(titles)):
        for row in rows:
            dict[titles[i]].append(row[i])

    return dict


transpose_users = partial(transpose, ["ID", "name", "age"])


transpose_books = partial(transpose, ["ID", "publisher", "name", "free", "date"])


transpose_bookshelfs = partial(transpose, ["ID", "name", "owner"])


class Controller:
    def __init__(self):
        self.model = Model()
        self.view = View()

        self.view.on("create", "user", self.create_user)
        self.view.on("update", "user", self.update_user)
        self.view.on("delete", "user", self.delete_user)
        self.view.on("show", "user", self.show_users)

        self.view.on("create", "book", self.create_book)
        self.view.on("update", "book", self.update_book)
        self.view.on("delete", "book", self.delete_book)
        self.view.on("show", "book", self.show_books)

        self.view.on("create", "bookshelf", self.create_bookshelf)
        self.view.on("update", "bookshelf", self.update_bookshelf)
        self.view.on("delete", "bookshelf", self.delete_bookshelf)
        self.view.on("show", "bookshelf", self.show_bookshelf)

        self.view.on_add_bookshelf_book(self.add_bookshelf_book)
        self.view.on_remove_bookshelf_book(self.remove_bookshelf_book)

    def loop(self):
        while True:
            try:
                self.view.wait_for_command()
            except psycopg2.Error as err:
                self.view.show_error(str(err).strip())
            except KeyboardInterrupt:
                return

    def generic_update(self, table: str, fields: Dict[str, Dict]):
        """
        { field_name: { nullable: bool, update: Callable, read: Callable } }
        """
        id = self.view.read_int("enter %s id" % table)
        if id is None:
            return

        while True:
            if len(fields) == 1:
                field = list(fields)[0]
            else:
                field = self.view.read_str("which field to change (%s)?" % "|".join(fields))
                if field == "":
                    return

            if field in fields:
                nullable = fields[field]["nullable"]
                update = fields[field]["update"]
                read = fields[field]["read"]

                if fields[field]["nullable"]:
                    action = self.view.read_str("field is nullable. change or delete it?")
                    if action == "delete":
                        update(id, None)
                        self.view.show_str("successfully deleted %s" % field)
                    elif action == "change":
                        value = read("enter new %s" % field)
                        if value is None or value == "":
                            return
                        update(id, value)
                        self.view.show_str("successfully updated %s" % field)
                    else:
                        self.view.show_error("invalid action")
                else:
                    value = read("enter new %s" % field)
                    if value is None or value == "":
                        return
                    update(id, value)
                    self.view.show_str("successfully updated %s" % field)
            else:
                self.view.show_error("invalid field")

    def create_user(self):
        username = self.view.read_str("enter username")
        if username == "":
            self.view.show_error("Username can not be empty")
            return
        age = self.view.read_str("enter age")

        id = self.model.create_user(username, age)
        self.view.show_table("user", {"id": [id], "username": [age], "email": [age]})

    def update_user(self):
        self.generic_update("user", {
            "username": {
                "nullable": False,
                "update": self.model.update_user_username,
                "read": self.view.read_str
            },
            "age": {
                "nullable": False,
                "update": self.model.update_user_age,
                "read": self.view.read_str
            }
        })


    def delete_user(self):
        id = self.view.read_int("enter user id to delete")
        if id is None:
            return
        self.model.delete_user(id)
        self.view.show_str("deletion successful")

    def show_users(self, field: Optional[str]):
        if field is None:
            self.view.show_table("users", transpose_users(self.model.read_users()))
        elif field == "id":
            id = self.view.read_int("enter user id")
            if id is None:
                return
            self.view.show_table(
                "user %s" % id,
                transpose_users([self.model.read_user_by_id(id)])
            )
        elif field == "name":
            username = self.view.read_str("enter username")
            if username is None:
                return
            self.view.show_table(
                "user %s" % username,
                transpose_users([self.model.read_user_by_name(username)])
            )
        elif field == "name not includes":
            word = self.view.read_str("enter word to exclude")
            if word == '':
                return
            self.view.show_table("users", transpose_users(self.model.read_users_username_not_includes(word)))
        else:
            self.view.show_error("invalid field")

    def create_book(self):
        publisher = self.view.read_int("enter publisher")
        if publisher is None:
            return
        name = self.view.read_str("enter book name")
        if name == "":
            return
        free = self.view.read_bool("make it free?")
        if free is None:
            return

        id = self.model.create_book(publisher, name, free)
        self.view.show_table("book", {
            "id": [id],
            "publisher": [publisher],
            "name": [name],
            "free": [free]
        })

    def update_book(self):
        self.generic_update("book", {
            "name": {
                "nullable": False,
                "update": self.model.update_book_name,
                "read": self.view.read_str
            },
            "public": {
                "nullable": False,
                "update": self.model.update_book_free,
                "read": self.view.read_bool
            }
        })

    def delete_book(self):
        id = self.view.read_int("enter book id to delete")
        if id is None:
            return
        self.model.delete_book(id)
        self.view.show_str("deletion successful")

    def show_books(self, field: Optional[str]):
        if field is None:
            self.view.show_table("books", transpose_books(self.model.read_books()))
        elif field == "id":
            id = self.view.read_int("enter book id")
            if id is None:
                return
            self.view.show_table(
                "book %s" % id,
                transpose_books([self.model.read_book_by_id(id)])
            )
        elif field == "publisher":
            publisher = self.view.read_int("enter publisher id")
            if publisher is None:
                return
            self.view.show_table("%s's books", transpose_books(self.model.read_books_by_publisher(publisher)))
        elif field == "bookshelf":
            bookshelf = self.view.read_int("enter bookshelf id")
            if bookshelf is None:
                return
            self.view.show_table("books on bookshelf %s" % bookshelf, transpose_books(self.model.read_books_by_bookshelf(bookshelf)))
        elif field == "free":
            free = self.view.read_bool("show free books?")
            if free is None:
                return
            if free:
                line = "free books"
            else:
                line = "not free books"
            self.view.show_table(line, transpose_books(self.model.read_books_by_free(free)))
        elif field == "date":
            date_range = self.view.read_date_range("enter upload date range")
            if date_range is None:
                return
            line = "books uploaded between %s and %s" % date_range
            self.view.show_table(line, transpose_books(self.model.read_books_by_date_range(date_range)))
        else:
            self.view.show_error("invalid field")


    def create_bookshelf(self):
        owner = self.view.read_int("enter owner")
        if owner is None:
            return
        name = self.view.read_str("enter bookshelf name")
        if name == "":
            return

        id = self.model.create_bookshelf(owner, name)
        self.view.show_table("bookshelf", {
            "id": [id],
            "name": [name],
            "owner": [owner]
        })

    def update_bookshelf(self):
        self.generic_update("bookshelf", {
            "name": {
                "nullable": False,
                "update": self.model.update_bookshelf_name,
                "read": self.view.read_str
            }
        })

    def delete_bookshelf(self):
        id = self.view.read_int("enter bookshelf id to delete")
        if id is None:
            return
        self.model.delete_bookshelf(id)
        self.view.show_str("deletion successful")

    def show_bookshelf(self, field: Optional[str]):
        if field is None:
            self.view.show_table("bookshelfs", transpose_bookshelfs(self.model.read_bookshelf()))
        elif field == "id":
            id = self.view.read_int("enter bookshelf id")
            if id is None:
                return
            self.view.show_table(
                "bookshelf %s" % id,
                transpose_bookshelfs([self.model.read_bookshelf_by_id(id)])
            )
        elif field == "owner":
            owner = self.view.read_int("enter owner id")
            if owner is None:
                return
            self.view.show_table(
                "%s's bookshelfs" % owner,
                transpose_bookshelfs(self.model.read_bookshelfs_by_owner(owner))
            )
        else:
            self.view.show_error("invalid field")

    def add_bookshelf_book(self):
        bookshelf = self.view.read_int("enter bookshelf id")
        if bookshelf is None:
            return
        book = self.view.read_int("enter book id")
        if book is None:
            return

        self.model.add_bookshelf_book(book, bookshelf)
        self.view.show_str("added book to bookshelf")

    def remove_bookshelf_book(self):
        bookshelf = self.view.read_int("enter bookshelf id")
        if bookshelf is None:
            return
        book = self.view.read_int("enter book id")
        if book is None:
            return

        self.model.remove_bookshelf_book(book, bookshelf)
        self.view.show_str("removed book from bookshelf")
