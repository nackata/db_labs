import psycopg2
from datetime import datetime
from typing import Dict, List, Optional, Union, Tuple


User = Tuple[int, str, int]
Publisher = Tuple[int, str, int]
Book = Tuple[int, str, int, datetime, bool]
Bookshelf = Tuple[int, str, int]


class Model:
    def __init__(self):
        self.conn = psycopg2.connect("dbname=lab_db password=Atickin00 user=postgres host=localhost")
        self.cur = self.conn.cursor()

    def create_user(self, username: str, age: int) -> int:
        self.cur.execute("INSERT INTO \"User\" (name, age) VALUES (%s, %s) RETURNING \"ID\";", (username, age))
        self.commit()

        return self.cur.fetchone()[0]

    def update_user_username(self, id: int, username: str):
        self.cur.execute("UPDATE \"User\" SET name = %s WHERE \"ID\" = %s;", (username, id))
        self.commit()

    def update_user_age(self, id: int, age: int):
        self.cur.execute("UPDATE \"User\" SET age = %s WHERE \"ID\" = %s;", (age, id))
        self.commit()

    def read_users(self) -> List[User]:
        self.cur.execute("SELECT \"ID\", name, age FROM \"User\";")
        return self.cur.fetchall()

    def read_user_by_id(self, id: int) -> User:
        self.cur.execute("SELECT \"ID\", name, age FROM \"User\" WHERE \"ID\" = %s;", (id,))
        return self.cur.fetchone()

    def read_users_username_not_includes(self, word: str) -> List[User]:
        self.cur.execute("SELECT \"ID\", name, age FROM \"User\" WHERE NOT name::tsvector @@ %s::tsquery;", (word,))
        return self.cur.fetchall()

    def read_users_username_includes_phrase(self, phrase: str) -> List[User]:
        self.cur.execute("SELECT \"ID\", name, age FROM \"User\" WHERE name::tsvector @@ %s::tsquery;", (" <-> ".join(phrase.split()),))
        return self.cur.fetchall()

    def read_user_by_name(self, name: str) -> User:
        self.cur.execute("SELECT \"ID\", name, age FROM \"User\" WHERE name = %s;", (name,))
        return self.cur.fetchone()

    def delete_user(self, id: int):
        self.cur.execute("DELETE FROM \"User\" WHERE \"ID\" = %s;", (id,))
        self.commit()


    def create_book(self, publisher: int, name: str, free: bool) -> int:
        self.cur.execute("INSERT INTO \"Book\" (publisher, name, free) VALUES (%s, %s, %s) RETURNING \"ID\";", (publisher, name, free))
        self.commit()

        return self.cur.fetchone()[0]

    def update_book_name(self, id: int, name: str):
        self.cur.execute("UPDATE \"Book\" SET name = %s WHERE \"ID\" = %s;", (name, id))
        self.commit()

    def update_book_free(self, id: int, free: bool):
        self.cur.execute("UPDATE \"Book\" SET public = %s WHERE \"ID\" = %s;", (free, id))
        self.commit()

    def read_books(self) -> List[Book]:
        self.cur.execute("SELECT \"ID\", publisher, name, free, date FROM \"Book\";")
        return self.cur.fetchall()

    def read_book_by_id(self, id: int) -> Book:
        self.cur.execute("SELECT \"ID\", publisher, name, free, date FROM \"Book\" WHERE \"ID\" = %s;", (id,))
        return self.cur.fetchone()

    def read_books_by_publisher(self, publisher: int) -> List[Book]:
        self.cur.execute("SELECT \"ID\", publisher, name, free, date FROM \"Book\" WHERE publisher = %s;", (publisher,))
        return self.cur.fetchall()

    def read_books_by_bookshelf(self, bookshelf: int) -> List[Book]:
        self.cur.execute("SELECT b.\"ID\", b.publisher, b.name, b.free, b.date FROM \"Book\" b INNER JOIN \"book_bookshelf\" p ON b.\"ID\" = p.book WHERE p.bookshelf = %s;", (bookshelf,))
        return self.cur.fetchall()

    def read_books_by_free(self, free: bool) -> List[Book]:
        self.cur.execute("SELECT \"ID\", publisher, name, free, date FROM \"Book\" WHERE free = %s;", (free,))
        return self.cur.fetchall()

    def read_books_by_date_range(self, date_range: Tuple[datetime, datetime]) -> List[Book]:
        self.cur.execute("SELECT \"ID\", publisher, name, free, date FROM \"Book\" WHERE date BETWEEN SYMMETRIC %s AND %s;", date_range)
        return self.cur.fetchall()

    def delete_book(self, id: int):
        self.cur.execute("DELETE FROM \"Book\" WHERE \"ID\" = %s;", (id,))
        self.commit()


    def create_bookshelf(self, owner: int, name: str) -> int:
        self.cur.execute("INSERT INTO \"virtual bookshelf\" (owner, name) VALUES(%s, %s) RETURNING \"ID\";", (owner, name))
        self.commit()

        return self.cur.fetchone()[0]

    def update_bookshelf_name(self, id: int, name: str):
        self.cur.execute("UPDATE \"virtual bookshelf\" SET name = %s WHERE \"ID\" = %s;", (name, id))
        self.commit()

    def read_bookshelf(self) -> List[Bookshelf]:
        self.cur.execute("SELECT \"ID\", name, owner FROM \"virtual bookshelf\";")
        return self.cur.fetchall()

    def read_bookshelf_by_id(self, id: int) -> Bookshelf:
        self.cur.execute("SELECT \"ID\", name, owner FROM \"virtual bookshelf\" WHERE \"ID\" = %s;", (id,))
        return self.cur.fetchone()

    def read_bookshelfs_by_owner(self, owner: id) -> List[Tuple[int, str, int]]:
        self.cur.execute("SELECT \"ID\", name, owner FROM \"virtual bookshelf\" WHERE owner = %s;", (owner,))
        return self.cur.fetchall()

    def delete_bookshelf(self, id: int):
        self.cur.execute("DELETE FROM \"virtual bookshelf\" WHERE \"ID\" = %s;", (id,))
        self.commit()

    def add_bookshelf_book(self, video: int, playlist: int):
        self.cur.execute("INSERT INTO \"video-playlist\" (video, playlist) VALUES (%s, %s)", (video, playlist))
        self.commit()

    def remove_bookshelf_book(self, video: int, playlist: int):
        self.cur.execute("DELETE FROM \"video-playlist\" WHERE video = %s AND playlist = %s;", (video, playlist))
        self.commit()


    def commit(self):
        self.conn.commit()
