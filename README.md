Матюха Микита Володимирович  
КП-72

## Лабороторна робота №1

[ER diagram](lab1/ER.png)  
[DB structure](lab1/DB_structure.png)  
[DB content example](lab1/DB_pgAdmin_example)  

## Лабороторна робота №2

Варіант 16

[DB structure](lab2/DB_structure.png)  
[DB gui example](lab2/DB_gui_example.png)  
[DB gui example](lab2/DB_gui_example2.png)  

## Лабороторна робота №3

[DB structure](lab3/DB_structure.png)  
