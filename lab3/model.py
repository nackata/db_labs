import psycopg2
import sqlalchemy
from sqlalchemy import Column, Integer, Text, Table, MetaData, types, Boolean
from datetime import datetime
from typing import Dict, List, Optional, Union, Tuple


User = Tuple[int, str, int]
Publisher = Tuple[int, str, int]
Book = Tuple[int, str, int, datetime, bool]
Bookshelf = Tuple[int, str, int]


class Model:
    def __init__(self):
        # self.conn = psycopg2.connect("dbname=lab_db password=Atickin00 user=postgres host=localhost")
        # self.cur = self.conn.cursor()

        self.engine = sqlalchemy.create_engine("postgres://postgres:Atickin00@localhost/lab_db")
        self.meta = MetaData()

        self.users = Table(
            'User', self.meta,
            Column('ID', Integer, primary_key=True),
            Column('name', Text),
            Column('age', Integer)
        )

        self.books = Table(
            'Book', self.meta,
            Column('ID', Integer, primary_key=True),
            Column('publisher', Integer),
            Column('name', Text),
            Column('date', types.DateTime),
            Column('free', Boolean)
        )

        self.bookshelfs = Table(
            'virtual bookshelf', self.meta,
            Column('ID', Integer, primary_key=True),
            Column('name', Text),
            Column('owner', Integer)
        )

        self.b_bs = Table(
            'book_bookshelf', self.meta,
            Column('ID', Integer, primary_key=True),
            Column('book', Integer),
            Column('bookshelf', Integer)
        )

        
    def create_user(self, username: str, age: int) -> int:
        stmt = self.users \
            .insert() \
            .values(name=username, age=age)

        return self.engine.execute(stmt).inserted_primary_key[0]
        

    def update_user_username(self, id: int, username: str):
        stmt = self.users \
            .update() \
            .where(self.users.c.ID == id) \
            .values(name=username)

        self.engine.execute(stmt)

    def update_user_age(self, id: int, age: int):
        stmt = self.users \
            .update() \
            .where(self.users.c.ID == id) \
            .values(age=age)

        self.engine.execute(stmt)

    def read_users(self) -> List[User]:
        stmt = self.users.select()

        return self.engine.execute(stmt).fetchall()

    def read_user_by_id(self, id: int) -> User:
        stmt = self.users.select().where(self.users.c.ID == id)

        return self.engine.execute(stmt).fetchone()

    def read_user_by_name(self, name: str) -> User:
        stmt = self.users.select().where(self.users.c.name == name)

        return self.engine.execute(stmt).fetchone()

    def delete_user(self, id: int):
        stmt = self.users.delete().where(self.users.c.ID == id)

        self.engine.execute(stmt)

    def create_book(self, owner: int, name: str, free: bool) -> int:
        stmt = self.books \
            .insert() \
            .values(publisher=owner, name=name, free=free)

        return self.engine.execute(stmt).inserted_primary_key[0]

    def update_book_name(self, id: int, name: str):
        stmt = self.books \
            .update() \
            .where(self.books.c.ID == id) \
            .values(name=name)

        self.engine.execute(stmt)

    def update_book_free(self, id: int, free: bool):
        stmt = self.books \
            .update() \
            .where(self.books.c.ID == id) \
            .values(free=free)

        self.engine.execute(stmt)

    def read_books(self) -> List[Book]:
        stmt = self.books.select()

        return self.engine.execute(stmt).fetchall()

    def read_book_by_id(self, id: int) -> Book:
        stmt = self.books.select().where(self.books.c.ID == id)

        return self.engine.execute(stmt).fetchone()

    def read_books_by_publisher(self, owner: int) -> List[Book]:
        stmt = self.books.select().where(self.books.c.publisher == owner)

        return self.engine.execute(stmt).fetchall()

    def read_books_by_bookshelf(self, bookshelf: int) -> List[Book]:
        stmt = sqlalchemy \
            .select([self.books]) \
            .select_from(self.books.join(self.b_bs, self.books.c.ID == self.b_bs.c.book)) \
            .where(self.b_bs.c.bookshelf == bookshelf)

        return self.engine.execute(stmt).fetchall()

    def read_books_by_free(self, free: bool) -> List[Book]:
        stmt = self.books.select().where(self.books.c.free == free)

        return self.engine.execute(stmt).fetchall()

    def read_books_by_date_range(self, date_range: Tuple[datetime, datetime]) -> List[Book]:
        stmt = self.books.select().where(sqlalchemy.between(self.books.c.date, date_range[0], date_range[1]))

        return self.engine.execute(stmt).fetchall()

    def delete_book(self, id: int):
        stmt = self.books.delete().where(self.books.c.ID == id)

        self.engine.execute(stmt)


    def create_bookshelf(self, owner: int, name: str) -> int:
        stmt = self.bookshelfs.insert().values(owner=owner, name=name)

        return self.engine.execute(stmt).inserted_primary_key[0]

    def update_bookshelf_name(self, id: int, name: str):
        stmt = self.bookshelfs.update().where(self.bookshelfs.c.ID == id).values(name=name)

        self.engine.execute(stmt)

    def read_bookshelf(self) -> List[Bookshelf]:
        stmt = self.bookshelfs.select()

        return self.engine.execute(stmt).fetchall()

    def read_bookshelf_by_id(self, id: int) -> Book:
        stmt = self.bookshelfs.select().where(self.bookshelfs.c.ID == id)

        return self.engine.execute(stmt).fetchone()

    def read_bookshelfs_by_owner(self, owner: id) -> List[Bookshelf]:
        stmt = self.bookshelfs.select().where(self.bookshelfs.c.owner == owner)

        return self.engine.execute(stmt).fetchall()

    def delete_bookshelf(self, id: int):
        stmt = self.bookshelfs.delete().where(self.bookshelfs.c.ID == id)
 
        self.engine.execute(stmt)

    def add_bookshelf_book(self, book: int, bookshelf: int):
        stmt = self.b_bs.insert().values(book=book, bookshelf=bookshelf)

        self.engine.execute(stmt)

    def remove_bookshelf_book(self, book: int, bookshelf: int):
        stmt = self.b_bs.delete().where(self.b_bs.c.book == book and self.b_bs.c.bookshelf == bookshelf)

        self.engine.execute(stmt)
